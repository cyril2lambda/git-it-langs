# ИТ в изучении иностранных языков

См. в папках, если относительные ссылки сломаны.

## Задания

- [Задание 1](./1/README.md)
- [Задание 2](./2/README.md)
- [Задание 3](./3/README.md)
- [Задание 4](./4/README.md)
- [Задание 5](./5/README.md)
- [Задание 6](./6/README.md)

## Диагностические задания

- [Задание 1. Юникод](./d1/README.md)
- [Задание 2. Анализ ЭОР](./d2/README.md)
- [Задание 3. Кнопка](./d3/README.md)
- [Задание 4. Анимация](./d4/README.md)

